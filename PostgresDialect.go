package migration

// PostgresDialect implements Dialect for the Postgres SQL dialect.
type PostgresDialect struct{}

var _ Dialect = &PostgresDialect{}

// SQLForCreateMigrationsTable provides the migration table creation SQL.
func (d *PostgresDialect) SQLForCreateMigrationsTable() string {
	return `
		CREATE TABLE IF NOT EXISTS migrationVersion (
			version BIGINT DEFAULT 0,
			PRIMARY KEY(version)
		);
	`
}

// SQLForInsertInitialMigrationVersion provides the SQL for the initial insert
// of the migration state into the database.
func (d *PostgresDialect) SQLForInsertInitialMigrationVersion() string {
	return `
		INSERT INTO migrationVersion (version) VALUES(0);
	`
}

// SQLForGetMigrationVersion provides the SQL for reading a migration version.
func (d *PostgresDialect) SQLForGetMigrationVersion() string {
	return `
		SELECT version FROM migrationVersion;
	`
}

// SQLForSetMigrationVersion provides the SQL for setting a migration version.
func (d *PostgresDialect) SQLForSetMigrationVersion() string {
	return `
		UPDATE migrationVersion SET version=$1;
	`
}
