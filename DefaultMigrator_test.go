package migration

import (
	"database/sql"
	"fmt"
	"math/rand"
	"os"
	"sync"
	"testing"
	"time"

	_ "github.com/mattn/go-sqlite3"
	"github.com/stretchr/testify/suite"
	"gitlab.com/ljpx/log"
)

type DefaultMigratorSuite struct {
	suite.Suite

	sut *DefaultMigrator

	db       *sql.DB
	tempFile string
}

func TestDefaultMigratorSuite(t *testing.T) {
	suite.Run(t, &DefaultMigratorSuite{})
}

func (suite *DefaultMigratorSuite) SetupTest() {
	var err error

	rand.Seed(time.Now().UnixNano())
	n := rand.Int63()
	suite.tempFile = fmt.Sprintf("migration_tests_%v.db", n)

	suite.db, err = sql.Open("sqlite3", fmt.Sprintf("file:%v", suite.tempFile))
	suite.Require().Nil(err)

	suite.sut = NewDefaultMigrator(suite.db, &SQLite3Dialect{}, log.NewDummy())

	m1 := &migration1{}
	m2 := &migration2{}

	suite.sut.Use(m2)
	suite.sut.Use(m1)

	suite.Require().Exactly(m1, suite.sut.migrations[0])
	suite.Require().Exactly(m2, suite.sut.migrations[1])
}

func (suite *DefaultMigratorSuite) TearDownTest() {
	suite.db.Close()
	suite.Require().Nil(os.Remove(suite.tempFile))
}

func (suite *DefaultMigratorSuite) TestShouldMigrateSuccessfully() {
	// Arrange and Act.
	err := suite.sut.Run()

	// Assert.
	suite.Require().Nil(err)
}

func (suite *DefaultMigratorSuite) TestShouldMigrateSuccessfullyWithCompetingThreads() {
	// Arrange.
	wg := &sync.WaitGroup{}
	ch := make(chan error)

	closure := func(wg *sync.WaitGroup, ch chan<- error) {
		ch <- suite.sut.Run()
		wg.Done()
	}

	// Act.
	wg.Add(3)

	go closure(wg, ch)
	go closure(wg, ch)
	go closure(wg, ch)

	go func() {
		wg.Wait()
		close(ch)
	}()

	for err := range ch {
		suite.Require().Nil(err)
	}
}

// -----------------------------------------------------------------------------

type migration1 struct{}

var _ Migration = &migration1{}

func (*migration1) Name() string {
	return "migration1"
}

func (*migration1) Version() uint64 {
	return 1
}

func (*migration1) Up(tx *sql.Tx) error {
	_, err := tx.Exec(`
		CREATE TABLE user (
			id INTEGER PRIMARY KEY,
			firstName TEXT
		);
	`)

	return err
}

func (*migration1) Down(tx *sql.Tx) error {
	_, err := tx.Exec(`
		DROP TABLE user;
	`)

	return err
}

type migration2 struct{}

var _ Migration = &migration2{}

func (*migration2) Name() string {
	return "migration2"
}

func (*migration2) Version() uint64 {
	return 2
}

func (*migration2) Up(tx *sql.Tx) error {
	_, err := tx.Exec(`
		CREATE TABLE address(
			id INTEGER PRIMARY KEY,
			street TEXT
		);
	`)

	return err
}

func (*migration2) Down(tx *sql.Tx) error {
	_, err := tx.Exec(`
		DROP TABLE address;
	`)

	return err
}
