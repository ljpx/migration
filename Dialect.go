package migration

// Dialect defines the methods that any SQL dialect must implement to use the
// migrator.
type Dialect interface {
	SQLForCreateMigrationsTable() string
	SQLForInsertInitialMigrationVersion() string

	SQLForGetMigrationVersion() string
	SQLForSetMigrationVersion() string
}
