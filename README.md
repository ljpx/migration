![](icon.png)

# migrate

Package `migrate` implements a basic database migration library.

## Usage Example

```go
type Migration1 struct{}

var _ Migration = &Migration1{}

func (*Migration1) Name() string {
    return "Migration1"
}

func (*Migration1) Version() uint64 {
    return 1
}

func (*Migration1) Up(tx *sql.Tx) error {
    _, err := tx.Exec(`
        CREATE TABLE user (
            id INTEGER PRIMARY KEY,
            firstName TEXT
        );
    `)

    return err
}

func (*Migration1) Down(tx *sql.Tx) error {
    _, err := tx.Exec(`
        DROP TABLE user;
    `)

    return err
}
```
