package migration

import "database/sql"

// Migration defines the methods that any migration must implement.
type Migration interface {
	Name() string
	Version() uint64

	Up(tx *sql.Tx) error
	Down(tx *sql.Tx) error
}
