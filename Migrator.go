package migration

// Migrator defines the methods that any migration implementation must
// implement.
type Migrator interface {
	Use(m Migration)
	Run() error
}
