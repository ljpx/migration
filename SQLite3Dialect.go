package migration

// SQLite3Dialect implements Dialect for the SQLite3 SQL dialect.
type SQLite3Dialect struct{}

var _ Dialect = &SQLite3Dialect{}

// SQLForCreateMigrationsTable provides the migration table creation SQL.
func (d *SQLite3Dialect) SQLForCreateMigrationsTable() string {
	return `
		CREATE TABLE IF NOT EXISTS migrationVersion (
			version INTEGER PRIMARY KEY DEFAULT 0
		);
	`
}

// SQLForInsertInitialMigrationVersion provides the SQL for the initial insert
// of the migration state into the database.
func (d *SQLite3Dialect) SQLForInsertInitialMigrationVersion() string {
	return `
		INSERT INTO migrationVersion (version) VALUES(0);
	`
}

// SQLForGetMigrationVersion provides the SQL for reading a migration version.
func (d *SQLite3Dialect) SQLForGetMigrationVersion() string {
	return `
		SELECT version FROM migrationVersion;
	`
}

// SQLForSetMigrationVersion provides the SQL for setting a migration version.
func (d *SQLite3Dialect) SQLForSetMigrationVersion() string {
	return `
		UPDATE migrationVersion SET version=?;
	`
}
