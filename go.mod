module gitlab.com/ljpx/migration

go 1.13

require (
	github.com/mattn/go-sqlite3 v1.10.0
	github.com/stretchr/testify v1.4.0
	gitlab.com/ljpx/id v1.0.0
	gitlab.com/ljpx/log v1.0.6
)
