package migration

import (
	"database/sql"
	"fmt"

	"gitlab.com/ljpx/id"
	"gitlab.com/ljpx/log"
)

// DefaultMigrator is the default implementation of Migrator.
type DefaultMigrator struct {
	sqlDB      *sql.DB
	dialect    Dialect
	logger     log.Logger
	migrations []Migration
}

var _ Migrator = &DefaultMigrator{}

// NewDefaultMigrator creates a new default migrator with the provided database
// connection, SQL dialect and logger.
func NewDefaultMigrator(sqlDB *sql.DB, dialect Dialect, logger log.Logger) *DefaultMigrator {
	return &DefaultMigrator{
		sqlDB:      sqlDB,
		dialect:    dialect,
		logger:     logger,
		migrations: make([]Migration, 0),
	}
}

// Use adds the provided migration to the list of migrations.
func (dm *DefaultMigrator) Use(m Migration) {
	var i int

	for i = 0; i < len(dm.migrations); i++ {
		if dm.migrations[i].Version() > m.Version() {
			break
		}
	}

	prefix := dm.migrations[:i]
	suffix := dm.migrations[i:]
	dm.migrations = append(prefix, append([]Migration{m}, suffix...)...)
}

// Run runs the set of migrations that have not yet been run.
func (dm *DefaultMigrator) Run() error {
	if len(dm.migrations) < 1 {
		return nil
	}

	if err := dm.ensureTableExists(); err != nil {
		return err
	}

	for i := 0; i < len(dm.migrations); i++ {
		targetMigration := dm.migrations[i]
		targetMigrationName := targetMigration.Name()
		targetMigrationVersion := targetMigration.Version()

		err := retryInDistinctSQLTransactions(dm.sqlDB, func(tx *sql.Tx) error {
			currentVersion, err := dm.getMigrationVersion(tx)
			if err != nil {
				return err
			}

			if currentVersion >= targetMigrationVersion {
				return nil
			}

			err = upDownUp(tx, targetMigration)
			if err != nil {
				return err
			}

			err = dm.setMigrationVersion(tx, targetMigrationVersion)
			if err != nil {
				return err
			}

			logMessage := fmt.Sprintf("Successfully migrated database to version %v ('%v')", targetMigrationVersion, targetMigrationName)
			dm.logger.Info(id.Empty, "Database Migration", logMessage)

			return nil
		})

		if err != nil {
			return err
		}
	}

	return nil
}

func (dm *DefaultMigrator) ensureTableExists() error {
	return retryInDistinctSQLTransactions(dm.sqlDB, func(tx *sql.Tx) error {
		_, err := tx.Exec(dm.dialect.SQLForCreateMigrationsTable())
		return err
	})
}

func (dm *DefaultMigrator) insertInitialMigrationVersion(tx *sql.Tx) error {
	return retryOnFirstError(func() error {
		_, err := tx.Exec(dm.dialect.SQLForInsertInitialMigrationVersion())
		return err
	})
}

func (dm *DefaultMigrator) getMigrationVersion(tx *sql.Tx) (uint64, error) {
	var version uint64

	row := tx.QueryRow(dm.dialect.SQLForGetMigrationVersion())
	err := row.Scan(&version)

	if err == sql.ErrNoRows {
		err = dm.insertInitialMigrationVersion(tx)
		if err != nil {
			return 0, err
		}

		return 0, nil
	}

	if err != nil {
		return 0, err
	}

	return version, nil
}

func (dm *DefaultMigrator) setMigrationVersion(tx *sql.Tx, version uint64) error {
	return retryOnFirstError(func() error {
		_, err := tx.Exec(dm.dialect.SQLForSetMigrationVersion(), version)
		return err
	})
}

func upDownUp(tx *sql.Tx, m Migration) error {
	err := m.Up(tx)
	if err != nil {
		return err
	}

	err = m.Down(tx)
	if err != nil {
		return err
	}

	return m.Up(tx)
}

func retryInDistinctSQLTransactions(db *sql.DB, closure func(tx *sql.Tx) error) error {
	return retryOnFirstError(func() error {
		tx, err := db.Begin()
		if err != nil {
			return err
		}
		defer tx.Rollback()

		err = closure(tx)
		if err != nil {
			return err
		}

		return tx.Commit()
	})
}

func retryOnFirstError(closure func() error) error {
	err := closure()

	if err != nil {
		err = closure()
	}

	return err
}
